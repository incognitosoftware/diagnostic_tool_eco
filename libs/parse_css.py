#!/usr/bin/env python

import json
import re
import tabulate
import text_formatter as tf

raw_response = open("/opt/diagnostic_tool_eco/files/services")
json = json.loads(raw_response.readlines()[0])
css_services = json["results"]

def color_service(status, line):
  status = status.lower()
  if "running" in status:
    line = tf.green(line)
  elif "starting" in status:
    line = tf.yellow(line)
  elif "paused" in status:
    line = tf.purple(line)
  elif any(state in status for state in ("stopped", "stopping")):
    line = tf.red(line)
  return line


# regex for capturing only IP from uris field
regex = re.compile(r'((\d{1,3}\.){3}\d{1,3})|(([\d\w]{2,4}:){7}[\d\w]{2,4})|((\w+\.)+\w+)')
serverDict = {}

for service in css_services:
    fqdn = service["serverName"]
    service_type = color_service(service["status"], service["type"])
    if fqdn not in serverDict:
      serverDict[fqdn] = {"ips": service["uris"], "services": [service_type]}
    else:
      serverDict[fqdn]["services"].append(service_type)

headers = ["Server Name", "Associated URIs", "Services"]
table = []
for fqdn in serverDict.keys():
  table.append([fqdn, '\n'.join(ip for ip in serverDict[fqdn]["ips"]), '\n'.join(service for service in serverDict[fqdn]["services"])])

print tabulate.tabulate(table, headers, tablefmt="grid")
statuses = ["running", "paused", "unknown", "starting", "stopped", "stopping"]
print "Legend: " + ' '.join(color_service(status, status) for status in statuses)