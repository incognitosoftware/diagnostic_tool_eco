#!/usr/bin/env python

class colours:
    PURPLE = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def __format( text, colour ):
    return "%s%s%s" % (colour, text, colours.ENDC)

def bold( text ):
    return __format(text, colours.BOLD)

def underlined( text ):
    return __format(text, colours.UNDERLINE)

def green( text ):
    return __format(text, colours.GREEN)

def red( text ):
    return __format(text, colours.RED)

def blue( text ):
    return __format(text, colours.BLUE)

def yellow( text ):
    return __format(text, colours.YELLOW)

def purple( text ):
    return __format(text, colours.PURPLE)
 
def boldGreen( text ):
    return bold(green(text))

def boldRed( text ):
    return bold(red(text))

def boldBlue( text ):
    return bold(blue(text))
    
def boldYellow( text ):
    return bold(yellow(text))
 
def boldPurple( text ):
    return bold(purple(text))
