#!/usr/bin/env python

# Prints each of the environment variables defined by the given
# script and their associated values.

import subprocess as sp
import string
import re
import sys
import os

# Matches to names of variables
regex = re.compile(r'(.+?)=')

environment_variables = []

def __shouldLookForVariablesInLine( line, getVarsStarted ):
    return (line.strip() == "_getvars()" or getVarsStarted)

def __addMatchToVariablesList( match ):
    for word in match:
        environment_variables.append(word.strip())

def __getVariableValuesList( f ):
  scriptName = os.path.basename(f)
  command = ['bash', '-c', 'source /opt/qatools/%s getvars; set -o posix ; set' % scriptName]
  proc = sp.Popen(command, stdout = sp.PIPE)
  return string.split(proc.communicate()[0], '\n')

def __printVariables( command_list ):
    print "Environment Variables :="
    for var in sorted(set(environment_variables)):
        for line in command_list:
            if var in line:
                print "%20s | %s" % ("$"+var, line.split("=")[1])

def display( f ):
    myfile = open(f, "r")
    getVarsStarted = False
    for line in myfile:
        if __shouldLookForVariablesInLine(line, getVarsStarted):
            getVarsStarted = True
            match = regex.findall(line)
            __addMatchToVariablesList(match)
            if line.strip() == "}":
               break

    command_list = __getVariableValuesList(f)
    __printVariables(command_list)

if __name__ == "__main__":
    display(sys.argv[1])
