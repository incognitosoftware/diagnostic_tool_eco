#!/usr/bin/python

# pass the parsing arguments as a list, Strings for keys or ints for indexes
# Usage: <json string> | ./json_parser.py key1 key2 0 key3 4

import io, sys
import json as js

def parse(keys):
  json = js.loads(sys.stdin.read())
  key = str(keys[0])
  for item in keys:
      try:
          json = json[int(item)]
      except ValueError:
          json = json[item]
  return json

if __name__ == '__main__':
  keys = sys.argv[1:]
  print parse(keys)