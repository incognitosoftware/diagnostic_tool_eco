#!/usr/bin/env python

import sys
import re
import tabulate
import text_formatter as tf

lines = sys.stdin.readlines()

table_headings = ["Service", "Version", "Last Started", "Status", "CSS Status", "License Expiration", "PID"]
rows = []
current_row = []

# match takes systemd implementation into consideration for ums_pkg
# Note that some systemd services use 'Main PID: ' instead of 'Process: '
pid_regex = re.compile(r'(?<!\d)\d{3,6}(?!-)(?!\d)')

def is_status_col(i):
  return i % 7 - 3  == 0

def is_connection_col(i):
  return i % 7 - 4 == 0

def is_last_col(i):
  return i % 7 - 6 == 0

def set_pid_col(line):
  match = pid_regex.findall(line)
  try:
    current_row.append(str(match[0]))
  except IndexError:
    current_row.append("--")
    # checkIfRunning(line)
    # check_if_found(line)

def check_if_found(line):
  if "".join(line.split()) == "":
    current_row[6] = "--"

def color_status_text(status):
  active_state = status.split()[0]
  if any(state == active_state for state in ("active", "reloading", "activating")):
    status = tf.green(status)
  elif any(state == active_state for state in ("failed", "error")):
    status = tf.red(status)
  return tf.bold(status)

def color_connection_text(status):
  status = status.lower()
  if status == "running":
    status = tf.green(status)
  elif any(state == status for state in ("paused", "starting")):
    status = tf.yellow(status)
  elif any(state == status for state in ("stopped", "stopping")):
    status = tf.red(status)
  return tf.bold(status)

for i, line in enumerate(lines):
  line = line.strip()
  if (is_status_col(i)):
    status = color_status_text(line)
    current_row.append(status)
  elif (is_connection_col(i)):
    connection = color_connection_text(line)
    current_row.append(connection)
  elif (is_last_col(i)):
    set_pid_col(line)
    rows.append(current_row)
    current_row = []
  else:
    current_row.append(line)
print tabulate.tabulate(rows, table_headings, tablefmt="grid")