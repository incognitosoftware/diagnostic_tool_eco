#!/bin/bash

printExtServiceTable()
{
  local output
  for ((i = 0; i < ${#service_names[@]}; i++)); do
    # echo "${service_names[i]} ${versions[i]} ${last_starteds[i]} ${css_statuses[i]} ${licenses[i]} ${server_names[i]}"
    output+="${service_names[i]}\n ${versions[i]}\n ${last_starteds[i]}\n ${css_statuses[i]}\n ${licenses[i]}\n"
  done

  echo ""
  printf "$output" 2>/dev/null | $LIBS/print_ext_service_table.py
}

printServiceTable()
{
  local output
  for ((i = 0; i < ${#services[@]}; i++)); do
    # echo "${friendlyNames[i]} ${versions[i]} ${start_times[i]} ${statuses[i]} ${css_statuses[i]} ${license_exps[i]} ${pids[i]}\n"
    output+="${friendlyNames[i]}\n ${versions[i]}\n ${start_times[i]}\n ${statuses[i]}\n ${css_statuses[i]}\n ${license_exps[i]}\n ${pids[i]}\n"
  done

  echo ""
  printf "$output" 2>/dev/null | $LIBS/print_service_table.py
}

# Returns a list of package versions 
getPackageVersions()
{
  getServicesFriendlyNames
  if isDebian; then
    dpkg-query --showformat='${Version}' --show $1
  else
    rpm -q --queryformat "%{VERSION}" $1
  fi
}

# Returns true (0) if given service is licensed, false (1) otherwise.
isLicensed()
{
  if [[ "incognito-$CLS incognito-$FMS incognito-$RIS" == *"$1"* || \
        "Central Lease Service Firmware Management Service Router Information Service" == *"$1"* ]]; then
    return 0;
  fi
  return 1;
}

# Retrieves license expiration from given service
getLicenseExpiration()
{
  if isLicensed $1; then
    local ip expiry_date
    expiry_date=$(echo $2 \
                | $HOMEDIR/libs/json_parser.py serviceInfo subscriptionExpiryDate 2>/dev/null ||
                echo $2 \
                | $HOMEDIR/libs/json_parser.py serviceInfo demoExpiryDate 2>/dev/null)
    date -d $expiry_date "+%B %d, %Y %T" 2>/dev/null || echo "--"
  else
    echo "--"
  fi
}

getServiceInfo()
{
  local protocol="http" port ip=$3
  if   [[ "$1" == *"$CLS" || "$1" == "Central Lease Service" ]]; then port=$CLS_PORT;
  elif [[ "$1" == *"$CAS" || "$1" == "Central Authentication Service" ]]; then 
    port=$CAS_PORT
    protocol=https
  elif [[ "$1" == *"$CSS" || "$1" == "Central Server Service" ]]; then 
    port=$CSS_PORT
    protocol=https
  elif [[ "$1" == *"$FMS" || "$1" == "Firmware Management Service" ]]; then port=$FMS_PORT;
  elif [[ "$1" == *"$RIS" || "$1" == "Router Information Service" ]]; then port=$RIS_PORT;
  elif [[ "$1" == *"$FA" || "$1" == "Firmware Agent" ]]; then port=$FA_PORT;
  elif [[ "$1" == *"$IES" || "$1" == "Incognito Ecosystem Scripting" ]]; then port=$IES_PORT;
  fi
  if [[ -z $port || -z $2 ]]; then return 1; fi

  curl -sk $protocol://$ip:$port/serviceinfo --header "Authorization:IncognitoCas auth=$2"
}

getServicesFriendlyNames()
{
  friendlyNames=()
  for i in ${INSTALLED_FRIENDLY[@]}; do friendlyNames+=("$i"); done

  services=()
  for i in ${INSTALLED[@]}; do services+=("incognito-$i"); done

  if [ ${#INSTALLED[@]} -eq 0 ]; then exit; fi
}

# Retrieves service connection status from LSS
getCssConnection()
{
  local service=$1
  if [ "$1" == "incognito-$FA" ]; then service="incognito-firmware-agent"; fi
  local name=$(python -c "print ' '.join('$service'.split('-')[1:]).title()")
  (TMP=$(cat /var/opt/incognito/local-server-service/localservices.txt | grep -A 2 "$name") && echo ${TMP##*_STATE_}) || echo "N/A"
}

# Retrieves service status from systemd
getFullStatus()
{
  local active_state=$(systemctl is-active $1)
  local sub_state=$(systemctl show -p SubState $1 | cut -d '=' -f 2-)
  echo "$active_state ($sub_state)"
}

getStartTime()
{
  local timestamp=$(systemctl show -p ActiveEnterTimestamp $1 | cut -d '=' -f 2-)
  date -d "$timestamp" "+%B %d, %Y %T" 2>/dev/null
}

# Get RabbitMQ connection status to ECO services
# check whether there are any errors in logs (disconnected) or if users are connected
# if Rabbit port is changed, connection won't be found
getRabbitConnection()
{
  # find installed ECO services that should be connected to Rabbit
  RABBITMQ_PORT=5672
  local eco_rabbit_cgroup installed_eco_rabbit eco pids_connected_to_rabbitmq
  eco_rabbit_cgroup=("centralleaseservice" "inco-iesd" "FMSStart")
  eco_rabbit_fnames=("CLS" "IES" "FMS")
  for service in ${eco_rabbit_fnames[@]}; do
    if [[ "${INSTALLED_FRIENDLY[@]}" == *"$service"* ]]; then
      installed_eco_rabbit+=("$service")
    fi
  done
  pids_connected_to_rabbitmq=$(ss -peanut | grep "$RABBITMQ_PORT.*java" | sed -ne 's/.*pid=\(.*\),fd.*/\1/p' | sort | uniq)

  local i=0
  for proc in ${eco_rabbit_cgroup[@]}; do
    ps $pids_connected_to_rabbitmq | grep $proc &>/dev/null
    if [ $? == 0 ]; then
      connected_eco_services+=("${installed_eco_rabbit[i]}")
    fi
    i=$i+1
  done
  
  # check for errors if not all services are connected
  if [[ ${#connected_eco_services[@]} -lt ${#eco_rabbit_cgroup[@]} ]]; then  _checkRabbitErrors; fi
}

_checkRabbitErrors()
{
  # RabbitMQ will report an error every 15s if it hasn't been resolved
  RABBITMQ_LOG="/var/log/rabbitmq/rabbit@$(hostname -s).log"

  local error_message error_message_date_string error_alert_date error_date
  # Rabbit has different format for logs depending on version
  error_date=$(egrep -i '=error|\[error\]' $RABBITMQ_LOG |  tail -n1 | awk \
  '{
    gsub("::"," ")
    if ($3 == "[error]") { print $1"T"$2 } 
    else print $3, $4
  }')
  error_message=$(egrep -i '=error|\[error\]' $RABBITMQ_LOG -a5 |  tail -n3)
  error_alert_date=$(date -d '15 sec ago' +"%s")
  error_date=$(date -d "$error_date" +"%s")

  if [ $error_date -ge $error_alert_date ]; then
    printf "\033[93mWARNING: Found error within the past 15s in RabbitMQ logs at $RABBITMQ_LOG: \n$error_message\033[0m\n"
  fi
}

# takes in a string separated by commas
processList()
{
  if [ $# -ne 1 ]; then
    echo "Must give a single list of comma-separated values"
    return 1
  fi
  echo $1 | tr "," "\n"

}

getPrivateIp()
{
  echo $(hostname -I | head -n1)
}

lssIsUp()
{
  local lss_stat
  lss_stat=$(getFullStatus incognito-$LSS)
  if [[ "$lss_stat" == "active"* ]]; then
    return 0
  fi
  return 1
}

cssIsUp()
{
  local css_stat
  css_response=$(curl -sk https://$CSS_IP:$CSS_PORT/authenticationservices)
  if [[ -n "$css_response" ]]; then
    return 0
  fi
  return 1
}

# Get OS
# returns:
# -deb for Debian
# -cent for CentOS
# -maybe additional to be added?
getOS()
{
  python -c "import platform; print(platform.dist()[0])"
}

getOSMajorVersion()
{
  python -c "import platform; print(platform.dist()[1][0])"
}

# Returns true (0) if debian. false (1) otherwise.
# if isDebian; then ...
isDebian()
{
  if [[ `getOS | grep deb` != "" ]]; then
    return 0
  else
    return 1
  fi
}

# Returns true (0) if cent. false (1) otherwise.
isCent()
{
  if [[ `getOS | grep cent` != "" ]]; then
    return 0
  else
    return 1
  fi
}

# Returns true (0) if solaris. false (1) otherwise.
isSolaris()
{
  if [[ `getOS | grep solaris` != "" ]]; then
    return 0
  else
    return 1
  fi
}

getJavaMajorVersion()
{
  version=$(java -version 2>&1 | awk -F '"' '/version/ {print $2}' | awk -F '.' '{print $2}')
  if [[ $version == "" ]]; then
    echo "None"
  else
    echo "$version"
  fi
}


# this function takes one argument
# $1 = the filename of a log file (with the absolute path) and
# omit the numbers that are normally in the file name,
#   e.g. assume you have log files in /usr/local/lib/ums/data/
#   and it is either ums1.log (most recent) or ums0.log (if logs haven't been rolled)
#   then you will perform: getRollingLog /usr/local/lib/ums/data/ums.log
#   and it will return the most recent log file
getRollingLog()
{
  python /opt/qatools/libs/get_rolling_log.py $1
}


# Checks if a certain package is installed.
# Returns 0 if installed, 1 if not
# $1 =  the package
isInstalled()
{
  if isDebian; then
    cmd='dpkg -l'
  else
    cmd='rpm -q'
  fi

  response=$($cmd $1 2>/dev/null)
  return $?
}

# Get the most recent catalina log.
getTomcatLog()
{
  local TOMCAT_LOG_DIR="/var/log/tomcat"
  local tomcatFile=$(ls $TOMCAT_LOG_DIR 2>/dev/null | grep catalina.[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9].lo | sort | tail -1)
  echo "$TOMCAT_LOG_DIR/$tomcatFile"
}