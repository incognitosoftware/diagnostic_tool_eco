#!/usr/bin/env python

import sys
import tabulate
import text_formatter as tf

lines = sys.stdin.readlines()

table_headings = ["Service", "Version", "Last Started", "CSS Status", "License Expiration"]
rows = []
current_row = []

def is_connection_col(i):
  return i % 5 - 3 == 0

def is_last_col(i):
  return i % 5 - 4 == 0

def last_col(line):
  global pid_regex
  match = pid_regex.findall(line)
  try:
      current_row.append(str(match[0]))
  except IndexError:
    current_row.append("--")
    check_if_found(line)

def check_if_found(line):
  global current_row
  if "".join(line.split()) == "":
    current_row[1] = "--"

def color_connection_text(status):
  status = status.lower()
  if status == "running":
    status = tf.green(status)
  elif status == "starting":
    status = tf.yellow(status)
  elif status == "paused":
    status = tf.purple(status)
  elif any(state == status for state in ("stopped", "stopping")):
    status = tf.red(status)
  return tf.bold(status)

for i, line in enumerate(lines):
  line = line.strip()
  if (is_connection_col(i)):
    connection = color_connection_text(line)
    current_row.append(connection)
  elif (is_last_col(i)):
    current_row.append(line)
    rows.append(current_row)
    current_row = []
  else:
    current_row.append(line)
print tabulate.tabulate(rows, table_headings, tablefmt="grid")