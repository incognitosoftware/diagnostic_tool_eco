#!/usr/bin/env python

# Parses the switch statement of the qatool scripts to generate usage.
# Also calls print_variables to parse _getvars() of the scripts to display
# the enviornment variables.

import text_formatter as tf
import print_variables
import re
import sys
import os

if len(sys.argv) != 2:
    print "Please provide a filename as an argument"
    sys.exit(-1)

myfile = open(sys.argv[1], "r")
regex = re.compile(r'^\s+[^\*\-]\w+\)')
commands_str = ""
commands_comments = []
commandFound = False

for line in myfile:
    if commandFound:
        commands_comments.append(line)
        commandFound = False
    match = regex.findall(line)
    for word in match:
        commands_str = commands_str + word[:len(word)-1]
        commandFound = True

commands_list =  commands_str.split()
usage_str = os.path.basename(sys.argv[1])[:-3] + " COMMAND <parameters>"

print "Usage:"
print "\t%s" % usage_str
print "where COMMAND :="
for idx, val in enumerate(commands_list):
    print "%28s | %s" % (tf.bold(val), commands_comments[idx].lstrip()[2:])

print_variables.display(sys.argv[1])
