#!/usr/bin/env python

import sys
import tabulate
import text_formatter as tf

lines = sys.stdin.readlines()

table_headings = ["Service", "Version", "Status", "Connection"]
rows = []
current_row = []

def is_status_col(i):
    return i % 4 - 2  == 0

def is_connection_col(i):
    return i % 4 - 3 == 0

def color_status_text(status):
    active_state = status.split()[0]
    if any(state == active_state for state in ("active", "reloading", "activating")):
        status = tf.green(status)
    elif any(state == active_state for state in ("failed", "error")):
        status = tf.red(status)
    return tf.bold(status)

# currently considers RabbitMQ connections only
def color_connection_text(found_connections):
    if found_connections == "--":
        return "--"
    available_connections = ["CLS ", "IES ", "FMS"]
    connections = ""
    for conn in available_connections:
        if conn.strip() in found_connections:
            connections += tf.boldGreen(u"\xb7" + conn)
        else:
            connections += tf.boldRed(u"\xd7" + conn)
    return connections

for i, line in enumerate(lines):
    line = line.strip()
    if (is_status_col(i)):
        status = color_status_text(line)
        current_row.append(status)
    elif (is_connection_col(i)):
        connection = color_connection_text(line)
        current_row.append(connection)
        rows.append(current_row)
        current_row = []
    else:
        current_row.append(line)

print tabulate.tabulate(rows, table_headings, tablefmt="grid")