#!/usr/bin/env python

import sys
from json_parser import parse

services_json = parse(["results"])
ip = sys.argv[1]
indexes = ""

for i, service in enumerate(services_json):
  if any(ip in s for s in service["uris"]):
    indexes += str(i) + ";"
print indexes