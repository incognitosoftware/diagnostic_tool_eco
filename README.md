# **Overview**

This tool script is intended to be used for helping Support and Development by aggregating Customer environment information and can be built upon for further improvements.

With a lot of functions pilfered from the existing QA tool script originally created by **Timothy Tang**, this script adds additional commands to display information specifically for ECO services. For future add-ons with other products, [qatools](https://bitbucket.org/incognitosoftware/qatools/src/master/) has scripts for AC, ACS, BCC, and SAC as well.

## **Installation**

The host machine must have, at the very least, LSS installed and connected to an active CSS host for the script to work properly. Please place the script directory in `/opt/`.

The source.me file adds an alias for the script so you don't have to type the entire path: `alias eco='/opt/diagnostic_tool_eco/ecotool.sh'`
```
cd /opt
git clone https://yourbitbucketname@bitbucket.org/incognitosoftware/diagnostic_tool_eco.git
echo ". /opt/diagnostic_tool_eco/source.me" >> ~/.bashrc
. /opt/diagnostic_tool_eco/source.me
```
Or, if you don't want the script to be sourced at every start-up:

```
cd /opt
git clone https://yourbitbucketname@bitbucket.org/incognitosoftware/diagnostic_tool_eco.git
. /opt/diagnostic_tool_eco/source.me
```
To add aliases to quickly stop, start, restart specific services:
```
echo ". /opt/diagnostic_tool_eco/files/.ecoaliases" >> ~/.bashrc
```
# **Functionality**

* `eco` without any arguments will produce available commands and descriptions, e.g.:

```
[root@qabox]# eco
Usage:
        ecotool COMMAND <parameters>
where COMMAND :=
                show | Display information of ECO services. Use with flag -h for more info.

                info | Display information on third-party services that supports the ECO stack.

                tail | Sends tail -f command with log of specified service.

                 ips | Lists server IPs and names of connected ECO services found by CSS.

             sysinfo | Display system information

             getvars | Retrieve the environment variables. This is called when source.me is sourced.

         installable | Checks system requirements for ECO services.

               start | Starts all installed ECO services (CAS, CSS, CSPS, CLS, LSS, FMS, FA, RIS).

                stop | Stops all installed ECO services.

             restart | Restarts all installed ECO services.
...
```
* The tools create logging variables so that you don't need to type the full path, e.g. `tail -F $clslog` or `vim $cssconfig`. The general format of the variable is `$productname(log|config)`, run `eco` for more details.
* Service connections with RabbitMQ in `eco info` will be marked as disconnected if the service isn't running, if there's an issue when connecting to the service (eg. login error), or if the service wasn't configured to connect to RabbitMQ.
* `eco ips` will require a login every time since data is being pulled from CSS. `eco show` displays local service and will only need CSS when retrieving license expirations.
* If the host machine doesn't have CSS running--no problem! As long as LSS is running and connected to an active CSS host, the script is able to retrieve the host's IP and query for service information.
* If sourced, `systemctl` functions to query status or start/stop/restart services will be aliased in a format like so: `startcls`, `stopcls`, `restartcls`, `clsstat`.

## **Login Parameters**
Some commands will require logging in to ECO. If a specific login is needed, please **use the same credentials as when logging in to ECO on GUI**.

|Login parameter|                                                                 |
|---------------|-----------------------------------------------------------------|
|-N or -n       |Username. Defaults to `admin`.                                   | 
|-P or -p       |Password. Defaults to `incognito`.                               |
|-S or -s       |Server IP. No default provided. You must specify the server name.|

## **Sample Output**
#### **Display ECO services on the current host:**
*If the user used to log in doesn't have the proper permissions, license expiration may not be shown.*

`eco show` and `eco show -n admin -p incognito` will display the same output.
```bash
[root@eco-cls ~]# eco show

+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| Service   | Version   | Last Started            | Status           | CSS Status   | License Expiration         |   PID |
+===========+===========+=========================+==================+==============+============================+=======+
| CLS       | 3.1.1.17  | April 22, 2021 15:21:03 | active (running) | running      | December 31, 2030 23:59:59 | 25108 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| CSS       | 2.1.2.4   | April 22, 2021 15:23:57 | active (exited)  | running      | --                         | 27262 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| CAS       | 2.1.3.3   | April 21, 2021 15:04:57 | active (exited)  | running      | --                         | 11673 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| FMS       | 2.3.3.1   | April 22, 2021 15:23:43 | active (running) | running      | January 31, 2022 23:59:59  | 26830 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| FA        | 2.2.0.2   | April 21, 2021 15:05:00 | active (running) | running      | --                         | 11746 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| LSS       | 2.1.1.2   | April 22, 2021 15:24:04 | active (running) | running      | --                         | 27650 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| IES       | 2.0.2.3   | April 22, 2021 15:23:29 | active (exited)  | running      | --                         | 26467 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| RIS       | 1.0.0.30  | April 21, 2021 15:05:08 | active (running) | running      | May 01, 2020 00:00:00      | 11964 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
```
Using a non-admin user:
```
[root@eco-cls ~]# eco show -n mikehawk -p tomatotown

+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| Service   | Version   | Last Started            | Status           | CSS Status   | License Expiration         |   PID |
+===========+===========+=========================+==================+==============+============================+=======+
| CLS       | 3.1.1.17  | April 22, 2021 15:21:03 | active (running) | running      | December 31, 2030 23:59:59 | 25108 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| CSS       | 2.1.2.4   | April 22, 2021 16:16:16 | active (exited)  | running      | --                         | 32137 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| CAS       | 2.1.3.3   | April 21, 2021 15:04:57 | active (exited)  | running      | --                         | 11673 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| FMS       | 2.3.3.1   | April 22, 2021 15:23:43 | active (running) | running      | --                         | 26830 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| FA        | 2.2.0.2   | April 21, 2021 15:05:00 | active (running) | running      | --                         | 11746 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| LSS       | 2.1.1.2   | April 22, 2021 15:24:04 | active (running) | running      | --                         | 27650 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| IES       | 2.0.2.3   | April 22, 2021 15:23:29 | active (exited)  | running      | --                         | 26467 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
| RIS       | 1.0.0.30  | April 21, 2021 15:05:08 | active (running) | running      | May 01, 2020 00:00:00      | 11964 |
+-----------+-----------+-------------------------+------------------+--------------+----------------------------+-------+
```
### **Display IPs of connected machines and their installed services found by CSS:**
*Services will be coloured depending on its status.*

`eco ips` and `eco ips -n admin -p incognito` will display the same output:

```
[root@cls ~]# eco ips
IP address(es) of current machine: 192.168.1.75 192.168.56.3 2001:569:7b98:2900:2ef:3a10:94ad:c714
+------------------+-------------------------------------+----------------------------------------+
| Server Name      | Associated URIs                     | Services                               |
+==================+=====================================+========================================+
| cls              | http://192.168.1.69:9232            | Central Lease Service                  |
|                  | http://192.168.56.104:9232          | DHCP Service                           |
|                  |                                     | License Management Service             |
|                  |                                     | Local Server Service                   |
+------------------+-------------------------------------+----------------------------------------+
| cls.vbox.com     | http://192.168.1.75:9232            | Central Lease Service                  |
|                  | http://192.168.56.3:9232            | DHCP Service                           |
|                  |                                     | License Management Service             |
|                  |                                     | Local Server Service                   |
+------------------+-------------------------------------+----------------------------------------+
| eco-cls.vbox.com | https://192.168.56.2:3141           | Central Authentication Service         |
|                  | https://localhost:3141              | Central Authentication Service Private |
|                  |                                     | Central Lease Service                  |
|                  |                                     | Central Server Service                 |
|                  |                                     | DHCP Service                           |
|                  |                                     | Firmware Agent                         |
|                  |                                     | Firmware Management Service            |
|                  |                                     | Incognito Ecosystem Scripting          |
|                  |                                     | License Management Service             |
|                  |                                     | Local Server Service                   |
|                  |                                     | Router Information Service             |
+------------------+-------------------------------------+----------------------------------------+
| bcc.vbox.com     | soap://192.168.56.4:8973            | CFM Proxy Service                      |
|                  | soap+ssl://192.168.56.4:7973        | CFM Service                            |
|                  | corba://corbaloc::192.168.56.4:9973 | DHCP Service                           |
|                  |                                     | DNS Service                            |
|                  |                                     | Local Server Service                   |
|                  |                                     | MP Service                             |
+------------------+-------------------------------------+----------------------------------------+
| eco-cls          | https://192.168.56.2:3141           | Central Authentication Service         |
|                  | https://localhost:3141              | Central Authentication Service Private |
|                  |                                     | Central Lease Service                  |
|                  |                                     | Central Server Service                 |
|                  |                                     | DHCP Service                           |
|                  |                                     | Firmware Agent                         |
|                  |                                     | Firmware Management Service            |
|                  |                                     | Incognito Ecosystem Scripting          |
|                  |                                     | License Management Service             |
|                  |                                     | Local Server Service                   |
|                  |                                     | Router Information Service             |
+------------------+-------------------------------------+----------------------------------------+
Legend: running starting paused stopped stopping
```
### **Display ECO services on an external host:**

Note: If no login is provided, `admin:incognito` will be used by default.


```
[root@cls ~]# eco show -s 192.168.56.2
Server name: 
eco-cls
eco-cls.vbox.com

+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Service                                | Version   | Last Started            | CSS Status   | License Expiration         |
+========================================+===========+=========================+==============+============================+
| Central Authentication Service         | 2.1.3.3   | April 21, 2021 18:05:04 | unknown      | --                         |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Central Authentication Service         | 2.1.3.3   | April 21, 2021 18:05:04 | running      | --                         |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Central Authentication Service Private | --        | April 21, 2021 18:05:04 | unknown      | --                         |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Central Authentication Service Private | --        | April 21, 2021 18:05:04 | running      | --                         |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Central Lease Service                  | 3.1.1.17  | April 22, 2021 18:21:09 | unknown      | January 01, 2031 02:59:59  |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Central Lease Service                  | 3.1.1.17  | April 22, 2021 18:21:09 | running      | January 01, 2031 02:59:59  |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Central Server Service                 | 2.1.2.4   | April 22, 2021 13:10:10 | unknown      | --                         |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Central Server Service                 | 2.1.2.4   | April 22, 2021 19:16:16 | running      | --                         |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| DHCP Service                           | --        | April 21, 2021 08:13:10 | unknown      | --                         |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| DHCP Service                           | --        | April 21, 2021 08:13:10 | running      | --                         |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Firmware Agent                         | 2.2.0.2   | April 21, 2021 11:05:00 | unknown      | May 01, 2020 03:00:00      |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Firmware Agent                         | 2.2.0.2   | April 21, 2021 11:05:00 | running      | May 01, 2020 03:00:00      |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Firmware Management Service            | 2.3.3.1   | April 22, 2021 18:22:44 | stopping     | February 01, 2022 02:59:59 |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Firmware Management Service            | 2.3.3.1   | April 23, 2021 11:54:36 | running      | February 01, 2022 02:59:59 |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Incognito Ecosystem Scripting          | 2.0.2.3   | April 22, 2021 18:22:04 | unknown      | --                         |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Incognito Ecosystem Scripting          | 2.0.2.3   | April 22, 2021 18:23:30 | running      | --                         |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Router Information Service             | 1.0.0.30  | April 21, 2021 11:05:09 | unknown      | May 01, 2020 03:00:00      |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
| Router Information Service             | 1.0.0.30  | April 21, 2021 11:05:09 | running      | May 01, 2020 03:00:00      |
+----------------------------------------+-----------+-------------------------+--------------+----------------------------+
```
### **Display services relevant to running the ECO stack:**
Note: If an ECO service that is able to communicate with RabbitMQ is not installed or not running, it will be marked with ×. If a service is unable to make a connection due to a login error, it will be marked with × and a warning message will appear.


```
[root@qabox]# eco info
WARNING: Found error within the past 15s in RabbitMQ logs at /var/log/rabbitmq/rabbit@qabox.log: 
                 {amqp_error,access_refused,
                             "PLAIN login refused: user 'incognito' - invalid credentials",
                             'connection.start_ok'}}
+-----------+-----------+------------------+----------------+
| Service   | Version   | Status           | Connection     |
+===========+===========+==================+================+
| RabbitMQ  | 3.3.5     | active (running) | ×CLS ×IES ·FMS |
+-----------+-----------+------------------+----------------+
| MongoDB   | 4.0.23    | active (running) | --             |
+-----------+-----------+------------------+----------------+
| Tomcat    | 7.0.76    | active (running) | --             |
+-----------+-----------+------------------+----------------+
```