#!/bin/bash

HOMEDIR=/opt/diagnostic_tool_eco
LIBS=$HOMEDIR/libs
CSS_SERVICES_FILE=$HOMEDIR/files/services
. $LIBS/ecotoolFunctions.sh

CLS="central-lease-service"
CSS="central-server-service"
CAS="central-authentication-service"
CSPS="central-server-polling-service"
FMS="firmware-management-service"
FA="firmwareagent"
LSS="local-server-service"
IES="ecosystem-scripting"
RIS="router-information-service"

CSS_PORT=3139
CAS_PORT=3141
FMS_PORT=6909
FA_PORT=6961
RIS_PORT=6954
CLS_PORT=9232
IES_PORT=8965

FRIENDLY=("CLS" "CSS" "CAS" "CSPS" "FMS" "FA"  "LSS" "IES" "RIS")
SERVICES="$CLS $CSS $CAS $CSPS $FMS $FA $LSS $IES $RIS"
INSTALLED=()
INSTALLED_FRIENDLY=()

SVC_DIR='/etc/opt/incognito'
LOG_DIR='/var/log/incognito'

USERNAME=""
PASSWORD=""
TARGET=""

## HELPERS ##
ANSI_WARN="\033[93m"
ANSI_END="\033[0m"

_checkRequirements()
{
  required_os="$1"
  if [[ $required_os != *"$os"* ]]; then
    echo "Not installable, OS is $os, requires $required_os"
  elif [[ "$java_version" != "7" ]]; then
    echo "Java 7 required, found $java_version"
  else
    echo "Installable"
  fi
}

_installable()
{
  os_distribution=$(getOS)
  os_major_version=$(getOSMajorVersion)
  os="$os_distribution $os_major_version"
  java_version=$(getJavaMajorVersion)

  result=$(_checkRequirements "centos 7 or debian 8")
  echo "CLS, FMS"
  echo "  --> $result"

  result=$(_checkRequirements "centos 7, or debian 8")
  echo "ECO Services"
  echo "  --> $result"
}

_getvars()
{
  clslog=$LOG_DIR/$CLS.log
  caslog=$LOG_DIR/$CAS.log
  csslog=$LOG_DIR/$CSS.log
  cspslog=$LOG_DIR/$CSPS.log
  fmslog=$LOG_DIR/$FMS.log
  ieslog=$LOG_DIR/incognito-$IES.log

  if isDebian; then
    lsslog=/var/log/syslog
  else
    lsslog=/var/log/messages
  fi

  falog=$lsslog
  rislog=$lsslog

  clsconfig=$SVC_DIR/$CLS.conf
  casconfig=$SVC_DIR/$CAS.conf
  cssconfig=$SVC_DIR/$CSS.conf
  cspsconfig=$SVC_DIR/$CSPS.conf
  fmsconfig=$SVC_DIR/$FMS.conf
  faconfig=$SVC_DIR/$FAd.conf
  lssconfig=$SVC_DIR/$LSS.conf
  iesconfig=$SVC_DIR/incognito-$IES.conf
  risconfig=$SVC_DIR/routerinformationd.conf

  CSS_IP=$(cat $lssconfig | grep css | awk -F '=' '{print $2}')
}

_getToken()
{
  for i in {0..6}
  do
    auth_services=$(curl -sk https://$CSS_IP:$CSS_PORT/authenticationservices)
    output=$(echo "$auth_services" | $LIBS/json_parser.py results $i 2>/dev/null)
    status=$(echo "$auth_services" | $LIBS/json_parser.py results $i status 2>/dev/null)
    echo $output | grep -i private &> /dev/null
    if [ $? -eq 0 ] && [ $status == "SERVICE_STATE_RUNNING" ]; then
      uri=$(echo "$auth_services" | $LIBS/json_parser.py results $i uris 0 2>/dev/null)
      break
    fi
  done

  response=$(echo '{"username":"'$USERNAME'","password":"'$PASSWORD'"}' | curl -X POST -d @- $uri/login --header "Content-Type:application/json" -ks)
  echo $response | $LIBS/json_parser.py authorization 2>/dev/null || printf "$ANSI_WARN$(echo $response | tr -d '{"}' | awk -F ':' '{print $2}')$ANSI_END"
}


_getInstalled()
{
  if isDebian; then
    local cmd="dpkg -l"
  else
    local cmd="rpm -q"
  fi

  local idx=0;
  for i in $SERVICES;
  do
    $cmd incognito-$i &>/dev/null
    if [ $? -eq 0 ]; then
      INSTALLED+=($i)
      INSTALLED_FRIENDLY+=(${FRIENDLY[idx]})
    fi
  idx=$idx+1
  done
}

_getCssConnections()
{
  token=$1
  if [[ $token == *" "* ]]; then
   echo $token > $CSS_SERVICES_FILE
  else
    curl -sk https://$CSS_IP:$CSS_PORT/services --header "Authorization:IncognitoCas auth=$token" > $CSS_SERVICES_FILE
  fi
}

_servicesSetup()
{
  if [[ -z $USERNAME && -z $PASSWORD || -z $USERNAME ]]; then
    USERNAME="admin"
    PASSWORD="incognito"
  elif [[ -z $PASSWORD ]]; then
    read -p "ECO password: " -s PASSWORD
    printf "\n"
  fi

  token=$(_getToken $TARGET)
  if [[ $token == *" "* ]]; then echo $token; exit; fi
}

## SERVICES ##

# Prints a table of available ECO services with its version, status, CSS status, license expiration, PID
# retrieved from connected CSS
_extServices()
{
  local services_json IFS found_services services
  local service_names css_statuses last_starteds licenses versions
  local service_name last_started license version

  _getCssConnections $token
  services_json=$(cat $CSS_SERVICES_FILE)

  services=$(echo $services_json | $LIBS/css_services.py $TARGET 2>/dev/null)
  IFS=';' read -ra found_services <<< "$services"
  for i in "${found_services[@]}"
  do
    server_name=$(echo $services_json | $LIBS/json_parser.py results $i serverName 2>/dev/null)
    service_name=$(echo $services_json | $LIBS/json_parser.py results $i type 2>/dev/null)
    css_status=$(echo $services_json | $LIBS/json_parser.py results $i status 2>/dev/null)
    last_started=$(echo $services_json | $LIBS/json_parser.py results $i lastStarted 2>/dev/null)
    service_info=$(getServiceInfo "$service_name" $token $CSS_IP)
    license="--" version="--"
    if [[ -n $service_info ]]; then 
      license=$(getLicenseExpiration "$service_name" "$service_info")
      version=$(echo "$service_info" | $LIBS/json_parser.py serviceInfo serviceRevision 2>/dev/null)
    fi

    server_names+=("$server_name")
    css_statuses+=(${css_status##*_STATE_})
    last_starteds+=("$(date -d $last_started "+%B %d, %Y %T")")
    service_names+=("$service_name")
    licenses+=("$license")
    versions+=("$version")
  done
  if [ ${#found_services[@]} -eq 0 ]; then 
    echo "CSS is unreachable or no installed services are found."
  else
    echo "Server name: "
    for server_name in "${server_names[@]}"; do echo "$server_name"; done | sort -u
    printExtServiceTable service_names[@] versions[@] last_starteds[@] css_statuses[@] licenses[@]
  fi
  yes | rm $CSS_SERVICES_FILE 2>/dev/null
}

# Prints a table of available local ECO services with its version, status, CSS status, license expiration, PID
# CSS status is retrieved locally from localservices.txt
# If LSS is down, CSS status and license expiration will show "--" for all services
_services()
{
  local versions start_times statuses css_statuses license_exps pids output
  local start_time status css_status license_exp pid

  if ! (lssIsUp && cssIsUp); then printf "$ANSI_WARN""WARNING: Stale or inaccurate CSS status may be shown because CSS or LSS is not running.""$ANSI_END"; fi

  for service in ${services[@]}; do
    versions+=($(getPackageVersions $service))
    status=$(getFullStatus $service)
    start_time="--" css_status="--" license_exp="--" pid="--"
    css_status=$(getCssConnection $service)

    if [[ "$status" == "active"* ]]; then
      start_time=$(getStartTime $service)
      service_info=$(getServiceInfo $service $token $CSS_IP)
      license_exp=$(getLicenseExpiration "$service" "$service_info")
      pid="$(systemctl status $service | egrep -io '.{0,6}(└─).{0,6}' || \
      systemctl show -p ExecStart $service | egrep -io 'pid=.{0,6}')"
    fi
    start_times+=("$start_time")
    css_statuses+=("$css_status")
    statuses+=("$status")
    license_exps+=("$license_exp")
    pids+=("$pid")
  done

  if [ ${#services[@]} -eq 0 ]; then 
    echo "No ECO services found on this machine!"
  else
    _checkRabbitErrors
    printServiceTable friendlyNames[@] versions[@] start_times[@] statuses[@] css_statuses[@] license_exps[@] pids[@]
  fi
}

## EXTERNAL ##

# Prints table containing information of external services needed by ECO
# includes RabbitMQ, MongoDB, Tomcat
_info()
{
  local output
  _getExternalServicesInfo "rabbitmq-server" "rabbitmq-server" "RabbitMQ"
  _getExternalServicesInfo "mongod" "mongodb-org-server" "MongoDB"
  _getExternalServicesInfo "tomcat" "tomcat" "Tomcat"
  printf "$output" 2>/dev/null | $LIBS/print_other_services_table.py
}

# Retrieves version, service status, and connection to ECO services from RabbitMQ
# returns row of information
# $1: service name, $2: package name, $3: friendly name
_getExternalServicesInfo()
{
  local version connections="--" status="$(getFullStatus $1)" pkg=$2
  if isDebian; then
    if [ "$pkg" == "tomcat" ]; then pkg="tomcat7" status="$(getFullStatus $pkg)"; fi
    version=$(dpkg-query --showformat='${Version}' --show $pkg)
  else
    version=$(rpm -q $pkg --queryformat "%{VERSION}")
  fi

  if [ "RabbitMQ" == "$3" ]; then
    connected_eco_services=()
    getRabbitConnection
    connections=${connected_eco_services[@]}
  fi
  output="$output $3\n $version\n $status\n ${connections[@]}\n"
}

## TAIL ##

_tail()
{
  _getvars
  local name=$1"log"
  tail -f ${!name}
}

## CSS ##

# List ECO services found by LSS
_ips()
{
  echo "IP address(es) of current machine: $(getPrivateIp)"
  _getCssConnections $token
  $HOMEDIR/libs/parse_css.py 2>/dev/null || printf "$(cat ${CSS_SERVICES_FILE})\n"
  if [[ ! -s $CSS_SERVICES_FILE ]]; then echo "CSS is unreachable or no installed services are found."; fi
  yes | rm $CSS_SERVICES_FILE 2>/dev/null
}

## SYSTEM INFORMATION ##

_sysInfo()
{
  printf "OS: $(getOS) $(getOSMajorVersion)\n"
  printf "Number of CPUs: $(nproc --all)\n"
  mem=$(grep MemTotal /proc/meminfo | awk '{ print $2}')
  printf "Total memory: $(echo $mem | python -c 'import sys; print("%dG" % round(float(sys.stdin.readline())/1000000))')\n"
  filesys=$(df -h / | tail -n1)
  printf "Total disk space on /: $(echo $filesys | awk '{ print $2}')\n"
  printf "Available memory on /: $(echo $filesys | awk '{ print $4}')\n"
}

# $1 = init script command
_allServices()
{
  if [ "$1" = "stop" ]; then
    task='stopping'
  else
    task=$1"ing"
  fi
  for i in ${INSTALLED[@]};
  do
    echo "$task $i..."
    systemctl $1 incognito-$i
  done
}

_stop()
{
  _allServices stop
}

_start()
{
  _allServices start
}

_restart()
{
  _allServices restart
}

_getInstalled
getServicesFriendlyNames
if [ "$1" == "shortlist" ]; then
  echo "start stop restart status getvars versions token installable"
  exit
fi

_printUsage()
{
  printf "Usage: eco $1 [options]\n"
  printf "  -N <UserName>\n"
  printf "  -P [Password] // if no password is supplied at the end, it will be prompted\n"
  if [ "$1" == "show" ]; then printf "  -S <Server>\n"; fi
  exit
}

_cases()
{
  command=$1
  while [[ ! -z $2 ]]
    do
    case $2 in
      -N | -n)
        shift
        USERNAME=$2
        ;;
      -P | -p)
        shift
        PASSWORD=$2
        ;;
      -S | -s) # will only be called for eco show
        shift
        if [[ "$command" == "show" && -n $2 ]]; then TARGET=$2;
        else _printUsage $command; exit; fi
        ;;
      *)
        _printUsage $command
        exit
        ;;
    esac
  shift
  done
}

case "$1" in
  show)
  # Display information of ECO services. Use with flag -h for more info.
    _getvars
    _cases $@
    _servicesSetup "show"
    if [[ -n $TARGET ]]; then _extServices; exit; fi
    _services; 
    ;;
  info)
  # Display information on third-party services that supports the ECO stack.
    _info
    ;;
  tail)
  # Sends tail -f command with log of specified service.
    _tail $2
    ;;
  ips)
  # Lists server IPs and names of connected ECO services found by CSS.
    _getvars
    _cases $@
    _servicesSetup "ips"
    _ips
    ;;
  sysinfo)
  # Display system information
    _sysInfo
    ;;
  getvars)
  # Retrieve the environment variables. This is called when source.me is sourced.
    _getvars
    ;;
  installable)
  # Checks system requirements for ECO services.
    _installable
    ;;
  start)
  # Starts all installed ECO services (CAS, CSS, CSPS, CLS, LSS, FMS, FA, RIS).
    _start
    ;;
  stop)
  # Stops all installed ECO services.
    _stop
    ;;
  restart)
  # Restarts all installed ECO services.
    _restart
    ;;
  *)
    $HOMEDIR/libs/get_usage.py $0
esac